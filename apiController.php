<?php

//Setting Post Variables
$originZipCode = $_POST['origin_zipCode_p1']."-".$_POST['origin_zipCode_p2'];
$destinationZipCode = $_POST['destination_zipCode_p1']."-".$_POST['destination_zipCode_p2']; //04037-003
$weight = $_POST['weight'];
$cost_of_goods = $_POST['cost_of_goods'];
$width  = $_POST['width'];
$height = $_POST['height'];
$length = $_POST['length'];

//curl Init
$curl = curl_init();

//curl options
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.intelipost.com.br/api/v1/quote",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "", 
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"origin_zip_code\":\"$originZipCode\",\"destination_zip_code\":\"$destinationZipCode\",\"volumes\":[{\"weight\":$weight,\"volume_type\":\"BOX\",\"cost_of_goods\":$cost_of_goods,\"width\":$width,\"height\":$height,\"length\":$length}],\"identification\":{\"session\":\"04e5bdf7ed15e571c0265c18333b6fdf1434658753\",\"page_name\":\"carrinho\",\"url\":\"http://www.intelipost.com.br/checkout/cart/\"}}",
  CURLOPT_HTTPHEADER => array(
    "api_key: 9009f95101bf48b01a50928a2a71ed1ae9083fc1d3c08439b0613dfc38e656c5",
    "content-type: application/json",
    "platform: intelipost-docs"
  ),
));

//curl exucute
$response = curl_exec($curl);

$err = curl_error($curl);
curl_close($curl);

if ($err) {
  echo "API Error #:" . $err;
} else {
  echo $response;
}